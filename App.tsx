/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import Navigation from './src/navigations/AppNavigator';
import { NavigationContainer } from '@react-navigation/native';
import { DefaultTheme, DarkTheme, Provider as PaperProvider } from 'react-native-paper';
import { store } from './src/stores/store';
import { StoreProvider } from 'easy-peasy';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#385723',
    accent: '#7DA06D',
    // background  : 'transparent',
    // text:"white"
  },

};
const App = () => {
  return (
    <StoreProvider store={store}>
    <PaperProvider theme={theme}>
      <NavigationContainer>
          <Navigation />
      </NavigationContainer>
    </PaperProvider>
    </StoreProvider>

  );
};

export default App;
