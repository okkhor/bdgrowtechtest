import * as React from 'react';
import { StyleSheet, Button, View, SafeAreaView } from 'react-native';
import { useNavigation } from "@react-navigation/native";

const HomeScreen = () => {
    const navigation: any = useNavigation();
    const Separator = () => (
        <View style={styles.separator} />
    );
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.fixToText}>
                <Button
                    title="Task 1"
                    onPress={() => navigation.navigate('task1')}
                />
                <Separator />
                <Button
                    title="Task 2"
                    color="#f194ff"
                    onPress={() => navigation.navigate('task2')}
                />
            </View>
        </SafeAreaView>
    );
};

export default HomeScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        marginHorizontal: 16,
    },
    title: {
        textAlign: 'center',
        marginVertical: 8,
    },
    fixToText: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        padding: 20
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
});
