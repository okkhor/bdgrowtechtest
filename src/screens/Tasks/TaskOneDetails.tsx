import React from 'react';
import { Text, SafeAreaView, StyleSheet, ActivityIndicator, View } from 'react-native';
import { useStoreActions, useStoreState } from '../../stores/hooks';
import { useRoute } from "@react-navigation/native";

const TaskOneDetails = () => {
  const quotesTick = useStoreState(state => state.instaforex.quotesTick);
  const loadingquotesTick = useStoreState(state => state.instaforex.loadingquotesTick);
  const routes:any = useRoute();
  return (
    <SafeAreaView style={{ flex: 1, }}>
      {loadingquotesTick && <View style={[styles.loader]}>
        <ActivityIndicator size="large" color="#00B482" />
      </View>}
      {!loadingquotesTick && quotesTick !== null && <View style={{ justifyContent: "center", alignItems: "center", marginTop: 20 }}>
        <Text style={[styles.text]}> Symbol: {quotesTick?.symbol}</Text>
        <Text style={[styles.text]}> Description: {routes?.params?.description}</Text>
        <Text style={[styles.text]}> Digits: {quotesTick?.digits}</Text>
        <Text style={[styles.text]}> Ask: {quotesTick?.ask}</Text>
        <Text style={[styles.text]}> Bid: {quotesTick?.bid}</Text>
        <Text style={[styles.text]}> Change: {quotesTick?.change}</Text>
        <Text style={[styles.text]}> Change 24 h: {quotesTick?.change24h}</Text>
      </View>}
      {!loadingquotesTick && quotesTick == null &&
        <View style={{ justifyContent: "center", alignItems: "center", marginTop: 20 }}>
          <Text style={{ fontSize: 25, color: 'red' }}> No Data Found</Text>
        </View>
      }
      <Text> </Text>
    </SafeAreaView>
  );
}

export default TaskOneDetails;

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    fontWeight:'bold'
  },
  loader: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: "row",
    padding: 10,
    marginTop: 50,
  },

});