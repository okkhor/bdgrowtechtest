import React from 'react';
import {View, Text, ActivityIndicator, StyleSheet, SafeAreaView} from 'react-native';
import io from 'socket.io-client';

const symbol = [
  'GOLD',
  'EURUSD',
  'GBPUSD',
  'USDJPY',
  'USDCHF',
  'USDCAD',
  'AUDUSD',
];

const TaskTwo = () => {
  const [data, setData] = React.useState<any>();
  const [msg, setMsg] = React.useState<any>();

  React.useEffect(() => {
    let arr: any = [];
    const client = io('https://qrtm1.ifxdb.com:8443/');
    symbol.map(item => arr.push({symbol: item}));
    client.on('connect', () => client.emit('subscribe', symbol));
    client.on('quotes', function (data: any) {
      setMsg(data.msg);
      arr.map(
        (item: any, index: any) =>
          item.symbol === data.msg.symbol && arr.splice(index, 1, data.msg),
      );
      setData(arr);
    });
    return () => {
      client.off('connect');
      client.off('quotes');
      client.off('subscribe');
    };
  }, []);

  return (
    <SafeAreaView style={{flex: 1}}>
      {data ? (
        data.map((item: any, index: any) => (
          <View key={index} style={{borderTopWidth: 1, borderTopColor: '#ccc'}}>
            <View style={styles.item}>
              <Text style={styles.text1}>{item.symbol}</Text>
              <Text style={styles.text1}>{item.ask}</Text>
              <Text
                style={[
                  styles.text2,
                  {color: item.change > 0 ? 'green' : 'red'},
                ]}>
                {item.change > 0 && '+'}
                {item.change ?? '-'}
              </Text>
              <Text
                style={[
                  styles.text2,
                  {color: item.change > 0 ? 'green' : 'red'},
                ]}>
                {item.ask && item.change ? item.change > 0 && '+' : '-'}
                {item.ask &&
                  item.change &&
                  (100 / (item.ask / item.change)).toFixed(4)}
                {item.ask && item.change && '%'}
              </Text>
              <Text
                style={{
                  color: item.change > 0 ? 'green' : 'red',
                  fontWeight: 'bold',
                }}>
                {item.change > 0 ? '↑' : '↓'}
              </Text>
            </View>
          </View>
        ))
      ) : (
        <View style={{marginTop: '20%'}}>
          <ActivityIndicator size="large" />
        </View>
      )}
    </SafeAreaView>
  );
};

export default TaskTwo;

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    paddingVertical: 12,
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  text1: {
    color: 'black',
    fontSize: 14,
    fontWeight: 'bold',
    width: '15%',
    marginHorizontal: '4%',
  },
  text2: {
    fontSize: 14,
    fontWeight: '500',
    width: '18%',
    marginRight: '4%',
  },
});
