import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet, FlatList, useWindowDimensions, ActivityIndicator, TextInput, Button, SafeAreaView } from 'react-native';
import { useNavigation } from "@react-navigation/native";
import { useStoreActions, useStoreState } from '../../stores/hooks';

const TaskOne = () => {
  const { height } = useWindowDimensions();
  const navigation: any = useNavigation();
  const quotesList = useStoreState(state => state.instaforex.quotesList);
  const loadingquotesList = useStoreState(state => state.instaforex.loadingquotesList);
  const fetchquotesList = useStoreActions(actions => actions.instaforex.fetchquotesList);
  const fetchquotesTick = useStoreActions(actions => actions.instaforex.fetchquotesTick);
  const [currentPage, setCurrentPage] = React.useState<number>(0);
  const [search, setSearch] = React.useState<string>('');
  const itemPerPage = 10;
  const [renderedData, setRenderedData] = React.useState<any>([]);

  React.useEffect(() => {
    fetchquotesList();
  }, []);

  React.useEffect(() => {
    if (loadingquotesList === false) setRenderedData(quotesList);
  }, [loadingquotesList]);


  function Item({ item, onPress, backgroundColor }: any) {
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.item, backgroundColor]}>
        <Text style={{ fontSize: 20, color:'#fff' }}>{item.symbol ?? ''}</Text>
        <Text style={{ fontSize: 15, color:'#fff' }}>{item.description ?? ''}</Text>
      </TouchableOpacity>
    );
  }

  function renderItem({ item }: any, backgroundColor = '#2296F3') {
    return (
      <Item
        item={item}
        backgroundColor={{ backgroundColor }}
        onPress={() => {
          fetchquotesTick(item?.symbol)
          navigation.navigate('Details', {
            description: item?.description
          })
        }}
      />
    );
  };

  const searcItems = (text: any) => {
    const newData = quotesList.filter((quotesList: any) => quotesList.symbol.toUpperCase().indexOf(text.toUpperCase()) > -1);
    setSearch(text);
    setCurrentPage(0);
    !text && setRenderedData(quotesList);
    setRenderedData(newData);
  };

  function Search() {
    return (
      <SafeAreaView style={{ paddingBottom: 10 }}>
        <View style={{
          paddingHorizontal: 8,
          marginTop: 5,
        }}>
          <TextInput
            style={styles.inputStyle}
            onChangeText={text => searcItems(text)}
            value={search}
            underlineColorAndroid="transparent"
            placeholder="Search"
          />
        </View>
      </SafeAreaView>
    );
  }

  return (
    <View style={{ justifyContent: 'flex-start' }}>
      {loadingquotesList && <View style={[styles.loader]}>
        <ActivityIndicator size="large" color="#00B482" />
      </View>}
      {!loadingquotesList && quotesList?.length > 0 &&
        <View>
          {Search()}
          <FlatList
            style={{ maxHeight: height * .65 }}
            data={renderedData.slice(currentPage * itemPerPage, currentPage * itemPerPage + itemPerPage)}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
          />
          <View style={{
            paddingHorizontal: 16,
            paddingBottom: 10,
            marginTop: 20,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',

          }}>
            <View style={{ paddingRight: 20 }}>
              <Button
                title="Prev"
                disabled={currentPage === 0}
                onPress={() => setCurrentPage(currentPage - 1)}
              />
            </View>
            <Text>
              {`page ${currentPage + 1}/${Math.ceil(renderedData.length / itemPerPage)}`}
            </Text>

            <View style={{ paddingLeft: 20 }}>
              <Button
                title="Next"
                disabled={currentPage === Math.ceil(renderedData.length / itemPerPage) - 1}
                onPress={() => setCurrentPage(currentPage + 1)}
              />
            </View>
          </View>
        </View>
      }
      {!loadingquotesList && quotesList?.length == 0 &&
        <View style={{ justifyContent: "center", alignItems: "center", marginTop: 20 }}>
          <Text style={{ fontSize: 25, color: 'red' }}> No Data Found</Text>
        </View>
      }
    </View>
  );
}

export default TaskOne;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loader: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: "row",
    padding: 10,
    marginTop: 50,
  },
  item: {
    shadowColor: '#000',
    padding: 16,
    marginVertical: 4,
    marginHorizontal: 16,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 22,
  },
  inputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 20,
    margin: 5,
    borderRadius: 8,
  },
});