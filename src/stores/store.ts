import { createStore } from 'easy-peasy';
import instaForexModel, { IInstaForexModel } from "./instaforex";

export interface IStoreModel {
    instaforex: IInstaForexModel,
} 

const storeModel:IStoreModel = {
    instaforex: instaForexModel,
};
export const store = createStore(storeModel);