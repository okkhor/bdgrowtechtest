import axios from "axios";
import { action, Action, Thunk, thunk } from "easy-peasy";

export interface IQuotesList {
    symbol: string,
    description: string,
    digits: number,
    trade: number,
    type: number,
}

export interface IQuotesTick {
    symbol: string,
    description: string,
    digits: number,
    trade: number,
    type: number,
}

export interface IError {
    message: string
    error: Error
}

export interface IInstaForexModel {
    quotesList: any;
    addquotesList: Action<IInstaForexModel, any>,
    fetchquotesList: Thunk<IInstaForexModel>,
    loadingquotesList: boolean;
    setloadingquotesList: Action<IInstaForexModel, boolean>,
   
    quotesTick: any;
    addquotesTick: Action<IInstaForexModel, any>,
    fetchquotesTick: Thunk<IInstaForexModel, any>,
    loadingquotesTick: boolean;
    setloadingquotesTick: Action<IInstaForexModel, boolean>,
    
    errors: IError[],
    addError: Action<IInstaForexModel, IError>,
}

const instaForexModel: IInstaForexModel = {
    quotesList: [],
    errors: [],
    loadingquotesList: false,
    
    addquotesList: action((state, payload) => {
        state.quotesList=payload
    }),
    setloadingquotesList: action((state, payload) => {
        state.loadingquotesList=payload
    }),
    addError: action((state, payload) => {
        // state.todos[payload.id] = payload
        state.errors.push(payload)
    }),
    fetchquotesList: thunk(async (actions) => {
        actions.setloadingquotesList(true)
        const url = `https://quotes.ifxdb.com/api/quotesList`;
        try {
            const fetchData = await axios.get(url)
            actions.addquotesList(fetchData.data?.quotesList);
            actions.setloadingquotesList(false);
          } catch (error) {
            console.log(error)
            actions.setloadingquotesList(false);
            actions.addquotesList([]);
          }
    }),

    quotesTick: null,
    loadingquotesTick: false,
    
    addquotesTick: action((state, payload) => {
        state.quotesTick=payload
    }),
    setloadingquotesTick: action((state, payload) => {
        state.loadingquotesTick=payload
    }),

    fetchquotesTick: thunk(async (actions, payload) => {
        actions.setloadingquotesTick(true)
        const url = `https://quotes.ifxdb.com/api/quotesTick?q=${payload}`;
        try {
            const fetchData = await axios.get(url)
            actions.addquotesTick(fetchData.data?.[0]);
            actions.setloadingquotesTick(false);
          } catch (error) {
            console.log(error)
            actions.setloadingquotesTick(false);
            actions.addquotesTick(null)
          }
    }),
    
};

export default instaForexModel;