import 'react-native-gesture-handler';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import TaskOne from '../screens/Tasks/TaskOne';
import TaskTwo from '../screens/Tasks/TaskTwo';
import TaskOneDetails from '../screens/Tasks/TaskOneDetails';
const { Screen, Navigator } = createStackNavigator();

export default function Navigation() {
  return (
    <Navigator>
      <Screen name="Bdgrowtech" component={HomeScreen} />
      <Screen name="task1" component={TaskOne} options={{title:"Task 1"}}/>
      <Screen name="task2" component={TaskTwo} options={{title:"Task 2"}}/>
      <Screen name="Details" component={TaskOneDetails} />
    </Navigator>
  );
}
